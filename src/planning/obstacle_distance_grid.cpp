#include <planning/obstacle_distance_grid.hpp>
#include <slam/occupancy_grid.hpp>


ObstacleDistanceGrid::ObstacleDistanceGrid(void)
: width_(0)
, height_(0)
, metersPerCell_(0.05f)
, cellsPerMeter_(20.0f)
{
}


void ObstacleDistanceGrid::setDistances(const OccupancyGrid& map)
{
    resetGrid(map);
    
    ///////////// TODO: Implement an algorithm to mark the distance to the nearest obstacle for every cell in the map.
    //ObstacleDistanceGrid distances;
    
    int w = map.widthInCells();
    int h = map.heightInCells();
    

    // Setting the open spaces to 0, occupied space to 1
    for ( int y = 0 ; y < h ; y++ ){
        for ( int x = 0 ; x < w ; x++){
            if ( map( x , y ) >= 0 ){
                operator()( x , y ) = 0;// distance from obstacle = 0 coz it is the obstacle
            }
           
            else{
                operator()( x , y ) = 1;
            }
        }
    }

    for ( int y = 0 ; y < h ; y++ ){
        for ( int x = 0 ; x < w ; x++){
            if ( operator()( x , y ) != 0 ){
                float temp = 100000000.0;
                for ( int Y = 0 ; Y < h; Y++ ){
                     for ( int X = 0 ; X < w ; X++){
                        if ( operator()( X , Y ) == 0 ){
                            
                            Point<double>  A(X,Y), B(x, y);
                            auto a = grid_position_to_global_position(A,map);
                            auto b = grid_position_to_global_position(B,map);
                            float d = distance_between_points(a,b);

                            if((d!=0.0) && (d<temp)){
                                temp = d;
                            }
                            

                           }
            
        }

    }
    

 operator()( x , y ) = temp;
}
}}}


bool ObstacleDistanceGrid::isCellInGrid(int x, int y) const
{
    return (x >= 0) && (x < width_) && (y >= 0) && (y < height_);
}


void ObstacleDistanceGrid::resetGrid(const OccupancyGrid& map)
{
    // Ensure the same cell sizes for both grid
    metersPerCell_ = map.metersPerCell();
    cellsPerMeter_ = map.cellsPerMeter();
    globalOrigin_ = map.originInGlobalFrame();

    // If the grid is already the correct size, nothing needs to be done
    if((width_ == map.widthInCells()) && (height_ == map.heightInCells()))
    {
        return;
    }
    
    // Otherwise, resize the vector that is storing the data
    width_ = map.widthInCells();
    height_ = map.heightInCells();
    
    cells_.resize(width_ * height_);
}
