#include <planning/astar.hpp>
#include <planning/obstacle_distance_grid.hpp>
#include <list>
#include <algorithm>
using namespace std;
//cp -r ../siyuan/botlab-f19/src/Eigen/ ./src/
bool compare(const cost* const & lhs, const cost* const & rhs){
    
	return lhs->f < rhs->f;
}


robot_path_t search_for_path(pose_xyt_t start, 
                             pose_xyt_t goal, 
                             const ObstacleDistanceGrid& distances,
                             const SearchParams& params)
{
    ////////////////// TODO: Implement your A* search here //////////////////////////
    
    robot_path_t path;
    pose_xyt_t currentNode;
    int ROW = distances.heightInCells()  , COL = distances.widthInCells();
    cost grid[ROW ][COL];
    for (int i=0; i<ROW; i++)     { 
        for (int j=0; j<COL; j++) { 
                grid[i][j].f = 100000.0;
                grid[i][j].g = 100000.0;
                grid[i][j].h = 100000.0;

        } 
    } 
    std::cout << "initialize grid"<<"\n";        
    cost *current = new cost;
    auto startCell = global_position_to_grid_cell(Point<double>(start.x, start.y), distances);
    auto goalCell = global_position_to_grid_cell(Point<double>(goal.x, goal.y), distances);
    cost *startC = new cost;
    startC->x = startCell.x;
    startC->y = startCell.y;
    startC->parent = NULL;
    startC->f = 0.0;
    startC->g = 0.0;
    startC->h = 0.0;

    std::list<cost*> open;
    open.push_back(startC);
    std::cout << "Goal Cell:"<<goalCell.x<<","<< goalCell.y<<"\n";
    while(open.size()>0){           
        current = open.front();  
    
        open.pop_front();   

        grid[current->x][current->y].close = 1;
        //std::cout << "Size "<< open.size() << "," << closed.size() << "/n";

        if( (current->x == goalCell.x) && (current->y == goalCell.y))
        {
            break;
        }
        //std::cout << "Step 1"<<"\n";        
        int x = current->x, y = current->y;
        for (int i =-1; i<2;i++){        
            for (int j = -1; j<2; j++){
                if ((x+i >= ROW) || (y+j>= COL) || (x+i <0) || (y+j<0)){
                    continue;
                }
                //std::cout << "Step 2"<<" "<<ROW<<" "<<COL<<" "<<x+i<<" "<<y+j<<"\n";
                if(!(distances(x+i,y+j) == 0) && !((i==0) && (j==0)) )
                { 
                    int dx=x+i,dy=y+j;
                    int is = 0;
                    if (grid[dx][dy].close == 1) is =1;
                    if(!is){
                        //std::cout << "Step 3"<<"\n";
                        cost *neighbor = new cost;
                        neighbor->parent = current;
                        neighbor->g = current->g + i + j;
                        neighbor->h = distance_between_points( Point<int>(dx,dy) , Point<int>(goalCell.x,goalCell.y) );
                        neighbor->f = neighbor->g + neighbor->h + 0.5/distances(dx,dy);
                        neighbor->x = dx;
                        neighbor->y = dy;
           
                        //std::cout << "Step 4"<<"\n";
                        if (neighbor->f < grid[dx][dy].f){
                            
                            //std::cout << "Condition"<<"\n";

                            is = 0;
                            for (std::list<cost*>::iterator it=open.begin(); it != open.end(); ++it){ 
                                if (((*it)->x == dx) && ((*it)->y == dy)){
                                    is =1;
                                }
                            }
                            if(!is){
                                grid[dx][dy].f = neighbor->f;
                                grid[dx][dy].g = neighbor->g;
                                grid[dx][dy].h = neighbor->h;
                                
                                open.push_back(neighbor);
                                open.sort(compare);                                     
                                //std::cout << "Added neighbour " << neighbor->x << "," << neighbor->y<<"\n";

                            }
                        }
                        else if (neighbor->f == grid[dx][dy].f){
                            if (neighbor->h < grid[dx][dy].h){
                            
                            //std::cout << "Condition"<<"\n";

                            is = 0;
                            for (std::list<cost*>::iterator it=open.begin(); it != open.end(); ++it){ 
                                if (((*it)->x == dx) && ((*it)->y == dy)){
                                    is =1;
                                }
                            }
                            if(!is){
                                grid[dx][dy].f = neighbor->f;
                                grid[dx][dy].g = neighbor->g;
                                grid[dx][dy].h = neighbor->h;
                                
                                open.push_back(neighbor);
                                open.sort(compare);                                     
                                //std::cout << "Added neighbour " << neighbor->x << "," << neighbor->y<<"\n";

                            }

                            }
                            
                        }
                    }  
                }
            }  
        }                  
    }  
    if (open.size() == 0){
        std::cout << "No path\n";
            
        
        return path; 

    }          
    while(current->parent !=NULL){
        path.utime = start.utime;
        //std::cout << "Step 5"<<"\n";
        auto tp = grid_position_to_global_position(Point<double>(current->x,current->y), distances);
        currentNode.x = tp.x;
        currentNode.y = tp.y;
        path.path.insert(path.path.begin(),currentNode);    
        path.path_length = path.path.size();      
        //std::cout << "Setting parent " << current->x << "," << current->y<<"\n";
        current = current->parent;        
    }
    return path ;
}

