#include <slam/mapping.hpp>
#include <slam/moving_laser_scan.hpp>
#include <slam/occupancy_grid.hpp>
#include <common/grid_utils.hpp>
#include <numeric>


Mapping::Mapping(float maxLaserDistance, int8_t hitOdds, int8_t missOdds)
: kMaxLaserDistance_(maxLaserDistance)
, kHitOdds_(hitOdds)
, kMissOdds_(missOdds)
{
}


void Mapping::updateMap(const lidar_t& scan, const pose_xyt_t& pose, const pose_xyt_t& previousPose, OccupancyGrid& map)
{
    //////////////// TODO: Implement your occupancy grid algorithm here ///////////////////////
    int stride=1;
    MovingLaserScan movingScan(scan, previousPose, pose, stride);
    for (auto& ray : movingScan){
        Mapping::breshenham(ray, pose, map);
    }

}
void Mapping::breshenham(const adjusted_ray_t& ray, const pose_xyt_t& pose, OccupancyGrid& map){

    float dist = ray.range;
    float theta = ray.theta;

    int x0=std::round(map.cellsPerMeter()*pose.x)+100; //to grid coordinates
    int y0=std::round(map.cellsPerMeter()*pose.y)+100; //to grid coordinates
    float x_raw = map.cellsPerMeter()*dist*std::cos(theta);
    float y_raw = map.cellsPerMeter()*dist*std::sin(theta);

    int x1 = x0 + std::round(x_raw);
    int y1 = y0 + std::round(y_raw);

    int dx = abs(x1-x0);
    int dy = abs(y1-y0);
    int sx = x0<x1 ? 1 : -1;
    int sy = y0<y1 ? 1 : -1;
    int err = dx-dy;
    int x = x0;
    int y = y0;

    while(x != x1 || y != y1){
        CellOdds prior = map.logOdds(x,y);
        if (prior>-127){
            map.setLogOdds(x,y,prior-Mapping::kMissOdds_);
        }
    int e2 = 2*err;
    if (e2 >= -dy){
        err -= dy;
        x += sx;
    }
    if (e2 <= dx){
        err += dx;
        y += sy;
    }
}
    CellOdds prior = map.logOdds(x,y);
    if (prior<125){
        map.setLogOdds(x,y,prior+Mapping::kHitOdds_);
    }

}