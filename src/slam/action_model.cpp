#include <slam/action_model.hpp>
#include <lcmtypes/particle_t.hpp>
#include <common/angle_functions.hpp>
#include <cassert>
#include <cmath>
#include <iostream>
#include <slam/readingcsv.hpp>

// class CSVIterator;

ActionModel::ActionModel(void)
{
    //////////////// TODO: Handle any initialization for your ActionModel /////////////////////////
    prevOdometry.x = 0.0f;
    prevOdometry.y = 0.0f;
    prevOdometry.theta = 0.0f;
    prevOdometry.utime = 0.0f;

    std::ifstream file("action.csv");
    int count = 0;
    std::string::size_type sz;     // alias of size_t
    
    for(CSVIterator loop(file); loop != CSVIterator(); ++loop)
    {
        if (count == 0) {
            alpha_1 = std::stof((*loop)[1], &sz);
            alpha_2 = std::stof((*loop)[2], &sz);
            alpha_3 = std::stof((*loop)[3], &sz);
            alpha_4 = std::stof((*loop)[4], &sz);
        }
        count ++;
    }
    /*
    alpha_1 = 0.0001;
    alpha_2 = 0.001;
    alpha_3 = 0.0008;
    alpha_4 = 0.0005;
*/
    std::cout << "alpha_1 = " << alpha_1 << std::endl;
    std::cout << "alpha_2 = " << alpha_2 << std::endl;
    std::cout << "alpha_3 = " << alpha_3 << std::endl;
    std::cout << "alpha_4 = " << alpha_4 << std::endl;
    
}


bool ActionModel::updateAction(const pose_xyt_t& odometry)
{
    ////////////// TODO: Implement code here to compute a new distribution of the motion of the robot ////////////////

    // Calculate deltas
    delta_y     = odometry.y - prevOdometry.y;
    delta_x     = odometry.x - prevOdometry.x;
    delta_theta = wrap_to_pi(odometry.theta - prevOdometry.theta);


    delta_rot1  = wrap_to_pi(std::atan2(delta_y, delta_x) - prevOdometry.theta);
    delta_trans = std::sqrt(std::pow(delta_x, 2) + std::pow(delta_y, 2));
    delta_rot2  = wrap_to_pi(delta_theta - delta_rot1);

    // Calculate stddev
    sample_rot1  = alpha_1 * std::pow(delta_rot1, 2) + alpha_2 * std::pow(delta_trans, 2);
    sample_trans = alpha_3 * std::pow(delta_trans, 2) + alpha_4 * std::pow(delta_rot1, 2) + alpha_4 * std::pow(delta_rot2, 2);
    sample_rot2  = alpha_1 * std::pow(delta_rot2, 2) + alpha_2 * std::pow(delta_trans, 2);
    /*
    std::cout<<"Mean "<<delta_rot1<<"\n";
    std::cout<<"Mean "<<delta_trans<<"\n";
    std::cout<<"Mean "<<delta_rot2<<"\n";
    std::cout<<"STD "<<sample_rot1<<"\n";
    std::cout<<"STD "<<sample_trans<<"\n";
    std::cout<<"STD "<<sample_rot2<<"\n";
    */
    // Pass to the previous value
    prevOdometry = odometry;

    if (delta_trans > thres_move) return true;
    else return false;
}


particle_t ActionModel::applyAction(const particle_t& sample)
{
    ////////////// TODO: Implement your code for sampling new poses from the distribution computed in updateAction //////////////////////
    // Make sure you create a new valid particle_t. Don't forget to set the new time and new parent_pose.
    // initialize sample
    particle_t newparticle;

    std::random_device rd; //creating random device

    std::mt19937 rs(rd()); //creating as a random source

    std::normal_distribution<float> GD_rot1(0.0f, sample_rot1);
    std::normal_distribution<float> GD_trans(0.0f, sample_trans);
    std::normal_distribution<float> GD_rot2(0.0f, sample_rot2);

    float test = GD_rot1(rs);
    sample_rot1_hat  = wrap_to_pi(delta_rot1 - test);
    sample_trans_hat = delta_trans - GD_trans(rs);
    sample_rot2_hat  = wrap_to_pi(delta_rot2 - GD_rot2(rs));
    //std::cout<<"Sampled "<<test<<"\n";

    // Action Model
    newparticle.pose.x = sample.pose.x + sample_trans_hat * std::cos(sample.pose.theta + sample_rot1_hat);
    newparticle.pose.y = sample.pose.y + sample_trans_hat * std::sin(sample.pose.theta + sample_rot1_hat);
    newparticle.pose.theta = wrap_to_pi(sample.pose.theta + sample_rot1_hat + sample_rot2_hat);
    newparticle.pose.utime = prevOdometry.utime;
    newparticle.parent_pose = sample.pose;
    newparticle.weight = sample.weight;
    return newparticle;
}
