#include <slam/sensor_model.hpp>
#include <slam/occupancy_grid.hpp>
#include <lcmtypes/particle_t.hpp>
#include <common/grid_utils.hpp>
#include <cmath>

SensorModel::SensorModel(void)
{
    // TODO: Handle any initialization needed for your sensor model
    /* This must include the initialization of the class members within the class
    */
}


double SensorModel::likelihood(const particle_t& sample, const lidar_t& scan, const OccupancyGrid& map)
{
    // TODO: Implement your sensor model for calculating the likelihood of a particle given a laser scan //////////
    // Do the moving scan transformation
    MovingLaserScan movingScan(scan, sample.parent_pose, sample.pose);
    double weight = 0;

    // plotRay(movingScan, map);

    // Obtain the global xy poses for all the lasers
    for (const auto& ray: movingScan) {
        if (ray.range > 3.0) continue;
        float ray_x, ray_y;
        ray_x = ray.origin.x + ray.range * std::cos(ray.theta);
        ray_y = ray.origin.y + ray.range * std::sin(ray.theta);
        Point<float> ray_pose_(ray_x, ray_y);
        auto origin_idx = global_position_to_grid_cell(ray.origin, map);
        auto ray_idx = global_position_to_grid_cell(ray_pose_, map);
        if (!map.isCellInGrid(ray_idx.x, ray_idx.y)) continue;
        int cross_hit = crossHit(origin_idx, ray_idx, map);
        CellOdds logOdds = map.logOdds(ray_idx.x, ray_idx.y);
        CellOdds likelihood = map.likelihoodOdds(ray_idx.x, ray_idx.y);
        weight += weightGen(logOdds, likelihood, cross_hit);
    }
    return weight / movingScan.size();
}


int SensorModel::crossHit(const Point<int>& origin_idx, const Point<int>& ray_idx, const OccupancyGrid& map) {
    /*
    * \return 0: Has No Hit
    * Which means has no hit all the time
    * \return 1: Has Continuous Hit
    * If the first one or two cell got hits
    * \return 2: Has Incontinuous Hit
    * If any where from the third cell got hits; or the first one not but second
    * one got hit.
    */
    int dx = std::abs(ray_idx.x - origin_idx.x);
    int dy = std::abs(ray_idx.y - origin_idx.y);
    int sx = origin_idx.x < ray_idx.x ? 1 : -1;
    int sy = origin_idx.y < ray_idx.y ? 1 : -1;
    int err = dx - dy;
    int x = ray_idx.x;
    int y = ray_idx.y;
    int count = 0;
    bool continuous = false, incontinuous = false, hit;
    while(x != origin_idx.x || y != origin_idx.y){
        int e2 = 2 * err;
        if (e2 >= -dy) {
            err -= dy;
            x -= sx;
        }
        if (e2 <= dx) {
            err += dx;
            y -= sy;
        }
        CellOdds prior = map.logOdds(x,y);
        hit = prior >= 122;
        if (hit) {
            if (count == 0) continuous = true;
            else if (count == 1) {
                if (!continuous) return 2;
            }
            else incontinuous = true;
        }
        count ++;
    }
    if (incontinuous) return 2;
    else if (continuous) return 1;
    else return 0;
}


double SensorModel::weightGen(const CellOdds& logOdds, const CellOdds& likelihood, const int& cross_hit) {
    auto intlogOdds = int(logOdds);
    auto intlikelihood = int(likelihood);
    auto plus_likelihood = intlikelihood + 128;
    if (cross_hit == 2) return 0.0;
    else if (cross_hit == 1) {
        if (intlogOdds > 122) return 0.3 * plus_likelihood; // Make it about 192
        else if (intlogOdds > -15) return 0.2 * plus_likelihood;
        else return 0.1 * plus_likelihood;
    }
    else if (cross_hit == 0) {
        return plus_likelihood;
    }
    return -1;
}

/* 
void SensorModel::plotRay(const MovingLaserScan &movingScan, const OccupancyGrid& map) {
    cv::Scalar colorLine(255); // Green
    int thicknessLine = 2;
    cv::Mat image(map.heightInCells(), map.widthInCells(), CV_8UC1, cv::Scalar(0));
    const std::vector<adjusted_ray_t>::const_iterator ftr = movingScan.end();
    auto prev_ray = *ftr;
    float ray_x, ray_y;
    ray_x = prev_ray.origin.x + prev_ray.range * std::cos(prev_ray.theta);
    ray_y = prev_ray.origin.y + prev_ray.range * std::sin(prev_ray.theta);
    Point<float> ray_pose_(ray_x, ray_y);
    auto ray_idx = global_position_to_grid_cell(ray_pose_, map);
    cv::Point prev_point(ray_idx.x, ray_idx.y);

    for (const auto& ray: movingScan) {
        if (ray.range > 3.0) continue;
        ray_x = ray.origin.x + ray.range * std::cos(ray.theta);
        ray_y = ray.origin.y + ray.range * std::sin(ray.theta);
        Point<float> ray_pose_(ray_x, ray_y);
        // auto origin_idx = global_position_to_grid_cell(ray.origin, map);
        auto ray_idx = global_position_to_grid_cell(ray_pose_, map);
        if (!map.isCellInGrid(ray_idx.x, ray_idx.y)) continue;
        cv::Point current_point(ray_idx.x, ray_idx.y);
        if (std::abs(ray.range - prev_ray.range) > 0.05) {
            prev_ray = ray;
            prev_point = current_point;
            continue;
        }
        cv::line(image, prev_point, current_point, colorLine, thicknessLine);
        prev_ray = ray;
        prev_point = current_point;
    }

    std::vector<cv::Point2f> corners;
    map.detectCorner(image, corners);
    // for (cv::Point2f corner : corners)
	// {
    //     cv::circle(image, corner, 3, cv::Scalar(128), -1, 8, 0);
	// 	std::cout << "corners:" << corner << std::endl;
	// }
    // cv::Mat flipped;
    // cv::flip(image, flipped, 0);
    // cv::imshow("new corners", flipped);
    // cv::waitKey(0);
} */