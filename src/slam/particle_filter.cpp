#include <slam/particle_filter.hpp>
#include <slam/occupancy_grid.hpp>
#include <lcmtypes/pose_xyt_t.hpp>
#include <cassert>
#include <iterator>
#include <chrono>


ParticleFilter::ParticleFilter(int numParticles)
: kNumParticles_ (numParticles)
{
    assert(kNumParticles_ > 1);
    posterior_.resize(kNumParticles_);
}


void ParticleFilter::initializeFilterAtPose(const pose_xyt_t& pose)
{
    ////////////// TODO: Implement your method for initializing the particles in the particle filter /////////////////
    unif_weight_ = 1.0 / kNumParticles_;
    posteriorPose_ = pose;
    std::cout << pose.x << " " << pose.y << " " << pose.theta << std::endl;
    std::cout << "Initial weight: " << unif_weight_ << std::endl;
    for (particle_t& particle : posterior_) {
        particle.pose = pose;
        particle.weight = unif_weight_;
    }
}


pose_xyt_t ParticleFilter::updateFilter(const pose_xyt_t&      odometry,
                                        const lidar_t& laser,
                                        OccupancyGrid&   map)
{
    // Only update the particles if motion was detected. If the robot didn't move, then
    // obviously don't do anything.
    bool hasRobotMoved = actionModel_.updateAction(odometry);
    if(hasRobotMoved)
    {
        map.updateLikelihoodMap();
        std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
        auto prior = resamplePosteriorDistribution();
        auto proposal = computeProposalDistribution(prior);
        posterior_ = computeNormalizedPosterior(proposal, laser, map);
        posteriorPose_ = estimatePosteriorPose(posterior_);
        std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
        std::chrono::duration<double> time_used = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        // std::cout << "Filter updating frequency: " << 1.0 / time_used.count() << " Hz. " << std::endl;
    }
    posteriorPose_.utime = odometry.utime;
    return posteriorPose_;
}


pose_xyt_t ParticleFilter::poseEstimate(void) const
{
    return posteriorPose_;
}


particles_t ParticleFilter::particles(void) const
{
    particles_t particles;
    particles.num_particles = posterior_.size();
    particles.particles = posterior_;
    return particles;
}


std::vector<particle_t> ParticleFilter::resamplePosteriorDistribution(void)
{
    //////////// TODO: Implement your algorithm for resampling from the posterior distribution ///////////////////
    double r = genResamplingRand();
    double sum_weight = 0.0;
    std::vector<particle_t> prior;
    for (const particle_t& particle : posterior_) {
        sum_weight += particle.weight;
        // std::cout << "W: " << sum_weight << "\t\t";
        while (r < sum_weight) {
            particle_t new_particle = particle;
            new_particle.weight = unif_weight_;
            prior.push_back(new_particle);
            r += unif_weight_;
        }
        // std::cout << "U: " << r << "\r";
        if (int(prior.size()) == kNumParticles_) break;
    }
    assert(prior.size() == posterior_.size());
    return prior;
}


std::vector<particle_t> ParticleFilter::computeProposalDistribution(const std::vector<particle_t>& prior)
{
    //////////// TODO: Implement your algorithm for creating the proposal distribution by sampling from the ActionModel
    std::vector<particle_t> proposal;
    for (const particle_t& particle: prior){
        proposal.push_back(actionModel_.applyAction(particle));
    }
    return proposal;
}


std::vector<particle_t> ParticleFilter::computeNormalizedPosterior(const std::vector<particle_t>& proposal,
                                                                   const lidar_t& laser,
                                                                   const OccupancyGrid&   map)
{
    //////////// TODO: Implement your algorithm for computing the normalized posterior distribution using the
    //////////// TODO: particles in the proposal distribution
    // Get all the non-uniformed weights
    std::vector<double> weights;
    std::vector<particle_t> posterior;
    for (particle_t particle : proposal) {
        double weight = sensorModel_.likelihood(particle, laser, map);
        double updated_weight = weight;
        particle.weight = updated_weight;
        weights.push_back(updated_weight);
        posterior.push_back(particle);
    }

    // Calculate the sum
    double sum_weight = 0.0;
    for (const double& weight : weights) sum_weight += weight;

    // Normalize the weights
    for (particle_t& particle : posterior) {
        particle.weight /= sum_weight;
        // std::cout << particle.weight << std::endl;
    }
    return posterior;
}


pose_xyt_t ParticleFilter::estimatePosteriorPose(const std::vector<particle_t>& posterior)
{
    //////// TODO: Implement your method for computing the final pose estimate based on the posterior distribution
    float ave_x = 0, ave_y = 0, ave_cos_theta = 0, ave_sin_theta = 0, ave_theta;
    int64_t ave_utime = 0;
    for (const particle_t& particle : posterior) {
        ave_x += particle.pose.x * particle.weight;
        ave_y += particle.pose.y * particle.weight;
        ave_cos_theta += std::cos(particle.pose.theta) * particle.weight;
        ave_sin_theta += std::sin(particle.pose.theta) * particle.weight;
        ave_utime += particle.pose.utime * particle.weight;
    }
    ave_theta = std::atan2(ave_sin_theta, ave_cos_theta);

    pose_xyt_t pose;
    pose.utime = ave_utime;
    pose.x = ave_x;
    pose.y = ave_y;
    pose.theta = ave_theta;
    return pose;
}


double ParticleFilter::genResamplingRand(void) {
    std::random_device rd; //creating random device
    std::mt19937 rs(rd()); //creating as a random source
    std::uniform_real_distribution<double> unif(0.0f, unif_weight_);
    double a_random_float = unif(rs);
    return a_random_float;
}